﻿using MVVMArchitecture.Models;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Windows.Input;

namespace MVVMArchitecture.ViewModels
{
    class CustomerViewModel : ObservableObject
    {
        private string _customerFirstName;
        private string _customerLastName;
        private int _customerAge;
        private string _customerGender;
        private Customer _selectedItem;

        private ObservableCollection<Customer> customers = new ObservableCollection<Customer>();

        public CustomerViewModel()
        {
            customers.Add(new Customer("Robel", "Fekadu", 20, "Male"));
            Customer.count++;
        }

        public string CustomerFirstName
        {
            get { return _customerFirstName; }
            set
            {
                _customerFirstName = value;
                RaisePropertyChangedEvent("CustomerFirstName");
            }
        }

        public string CustomerLastName
        {
            get { return _customerLastName; }
            set
            {
                _customerLastName = value;
                RaisePropertyChangedEvent("CustomerLastName");
            }
        }

        public int CustomerAge
        {
            get { return _customerAge; }
            set
            {
                _customerAge = value;
                RaisePropertyChangedEvent("CustomerAge");
            }
        }

        public string CustomerGender
        {
            get { return _customerGender; }
            set
            {
                _customerGender = value;
                RaisePropertyChangedEvent("CustomerGender");
            }
        }

        public Customer SelectedCustomer
        {
            get { return _selectedItem; }
            set
            {
                _selectedItem = value;
                CustomerFirstName = _selectedItem.FirstName;
                CustomerLastName = _selectedItem.LastName;
                CustomerAge = _selectedItem.Age;
                CustomerGender = _selectedItem.Gender;
                RaisePropertyChangedEvent("SelectedCustomer");
            }
        }

        public ObservableCollection<Customer> Customers
        {
            get { return customers; }
        }

        public ICommand AddCustomerCommand
        {
            get { return new DelegateCommand(AddCustomer); }
        }

        private void AddCustomer()
        {
            if (string.IsNullOrWhiteSpace(CustomerFirstName) || string.IsNullOrWhiteSpace(CustomerFirstName) || CustomerAge == 0 || string.IsNullOrWhiteSpace(CustomerGender)) return;
            customers.Add(new Customer(CustomerFirstName, CustomerLastName, CustomerAge, CustomerGender));
            CustomerFirstName = string.Empty;
            CustomerLastName = string.Empty;
            CustomerAge = 0;
            CustomerGender = string.Empty;
            Customer.count++;
        }

        public ICommand EditCustomerCommand
        {
            get { return new DelegateCommand(EditCustomer); }
        }

        public void EditCustomer()
        {

            int id = SelectedCustomer.Id;
            foreach (Customer customer in Customers)
            {
                if (customer.Id == id)
                {
                    customer.FirstName = CustomerFirstName;
                    customer.LastName = CustomerLastName;
                    customer.Age = CustomerAge;
                    customer.Gender = CustomerGender;
                    customers.CollectionChanged += this.CustomersChanged;
                    break;
                }
            }

        }

        void CustomersChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            ObservableCollection<Customer> obsSender = sender as ObservableCollection<Customer>;
            NotifyCollectionChangedAction action = e.Action;
        }
    }
}
