﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MVVMArchitecture.Models
{
    public class Customer
    {
        public int Id { get; set; }
        public String FirstName { get; set; }
        public String LastName { get; set; }
        public int Age { get; set; }
        public String Gender { get; set; }

        public static int count = 0;

        public Customer(string firstName, string lastName, int age, string gender)
        {
            Id = count;
            FirstName = firstName;
            LastName = lastName;
            Age = age;
            Gender = gender;
        }
        public override string ToString()
        {
            return String.Format($"Name: {FirstName} {LastName} --- Gender: {Gender} --- Age: {Age}");
        }
    }
}
